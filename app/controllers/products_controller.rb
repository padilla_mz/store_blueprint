class ProductsController < ApplicationController
  
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  # before_filter :check_super

  # GET /products
  # GET /products.json
  def index
    @body_class = 'admin'
    @products = Product.default_admin

    @st = Product.ransack(params[:q])
    @products = @st.result(distinct: true).default_admin.paginate(per_page: 50, page: params[:page])
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @body_class = 'admin'
  end

  # GET /products/new
  def new
    @body_class = 'admin'
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
    @body_class = 'admin'
  end

  # POST /products
  # POST /products.json
  def create
    # @product = Product.new(product_params)

    @product = current_user.products.new(product_params)

    # if params[:product][:tags]
    #   puts "true sdfksjdfhkjsdhkjsdfh"
    # end
    # @product << product_params[:tags]


    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    puts "-" * 50
    puts params[:product][:card_number] + "\n \n \n"
    puts "-" * 50

    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:title, :content, :published, :published_at, :sku, :weight, :price, :cost_price, :featured, :stock, :category_id, :ancho, :perfil, :rin, :tire_brand, :tire_model_brand, :performance_km, :performance_cold, :performance_dry, :performance_wet, :brand_id, tag_ids: [], year_ids: [] )
    end
end
