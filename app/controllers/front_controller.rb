class FrontController < ApplicationController
  before_action :set_product, only: [:show_product]
  skip_before_action :authenticate_user!



  def index

    if params[:tag]
      @products = Product.tagged_with(params[:tag]).where(published: true).paginate(per_page: POSTS_PER_PAGE, page: params[:page])
    else
    	@st = Product.ransack(params[:q])
      @products = @st.result(distinct: true).default_admin.paginate(per_page: POSTS_PER_PAGE, page: params[:page])
    end
  end

  def show_product
  	@line_item = LineItem.new
  end

 

  private
  	def set_product
  		@product = Product.find(params[:id])
  	end
end
