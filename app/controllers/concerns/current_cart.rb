module CurrentCart
	extend ActiveSupport::Concern 

	def check_super
		redirect_to(root_path, alert: "you don't have authorization to do that") unless current_user.is? :super
  end

	private
		def set_cart
			@cart = Cart.find(session[:cart_id])
		rescue ActiveRecord::RecordNotFound
			@cart = Cart.create
			session[:cart_id] = @cart.id 
		end
	
end