

SITE_NAME = "sotollantas"
SITE_TITLE = "sotollantas"
SITE_URL = "http://sotollantas.com/"
META_DESCRIPTION = "Sito sotollantas"
META_KEYWORDS = ""
EMAIL = "info@sotollantas.com"
# EMAIL2 = "contacto@holayadira.com"

#will_paginate
POSTS_PER_PAGE = 30

#Locale
TWITTER_LOCALE = "en" #default "en"
FACEBOOK_LOCALE = "en_US" #default "en_US"
GOOGLE_LOCALE =  "es-419" #default "en", espanol latinoamerica "es-419"

#ANALYTICS
GOOGLE_ANALYTICS_ID = "" #your tracking id

#SOCIAL

DISQUS_SHORTNAME = ""
TWITTER_USERNAME1 = "@holaarandas"
FACEBOOK_URL2 = "https://www.facebook.com/pages/Hola-Arandas/473568116128166"
YOUTUBE = "https://www.youtube.com/channel/UCL61a9rA7ey_V460Yd7RYqg"

# GOOGLE_PLUS_URL = ""

