Rails.application.routes.draw do
  # resources :charges
  # resources :carts
  # resources :years
  # resources :line_items
  # resources :products

  scope '/admin' do
    resources :categories, :tags, :products, :years, :brands, :carts, :charges

    resources :line_items do
      put 'decrease', on: :member
      put 'increase', on: :member
    end
  end

  # get 'front/index'

  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'front#index'
  get 'products/:id', to: 'front#show_product', as: :show_product

  get "admin", to: 'admin#charts'

  # get "admin/charges" => 'charges#index'
  # get "admin/statics" => 'statics#index'
  # get "admin/promos" => 'promos#index'
  get "admin/users/defaults-users" => 'admin#n_users', as: :n_users
  get "admin/users/admins-users" => 'admin#a_users', as: :a_users

  ## Admin user profile
  get "admin/user/new" => 'admin#new_user', as: :new_user
  post "admin/user" => 'admin#create_user'

  get "admin/user/:id" => 'admin#show_user', as: :admin_show_user

  # to update role
  get "admin/user/:id/edit" => 'admin#edit', as: :user_edit
  delete "admin/user/:id" => 'admin#destroy_user', as: :user_delete
  
  patch "admin/user/:id" => 'admin#update'
  put "admin/user/:id" => 'admin#update', as: :user_edit_s

  get 'tags/:tag', to: 'front#index', as: :tag_search


  # new admin_user



  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
