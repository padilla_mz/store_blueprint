# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150601182412) do

  create_table "brands", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
    t.text     "desc"
  end

  create_table "carts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "charges", force: :cascade do |t|
    t.string   "name"
    t.text     "full_address"
    t.string   "colonia"
    t.string   "city"
    t.string   "country"
    t.string   "state"
    t.integer  "zip_code"
    t.integer  "phone_number"
    t.string   "status",           default: "en proceso"
    t.string   "ship_number"
    t.string   "delivery_service", default: "FedEx"
    t.string   "conekta_id"
    t.string   "livemode"
    t.string   "conekta_status"
    t.integer  "amount"
    t.string   "currency"
    t.string   "payment_type"
    t.string   "payment_barcode"
    t.string   "payment_bar_url"
    t.integer  "user_id"
    t.integer  "counter",          default: 0
    t.boolean  "stock_discounted", default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "charges", ["user_id"], name: "index_charges_on_user_id"

  create_table "line_items", force: :cascade do |t|
    t.integer  "cart_id"
    t.integer  "quantity",                           default: 1
    t.decimal  "price",      precision: 8, scale: 2
    t.integer  "charge_id"
    t.string   "property"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "product_id"
  end

  add_index "line_items", ["cart_id"], name: "index_line_items_on_cart_id"
  add_index "line_items", ["charge_id"], name: "index_line_items_on_charge_id"
  add_index "line_items", ["product_id"], name: "index_line_items_on_product_id"

  create_table "products", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "content"
    t.boolean  "published",                                default: true
    t.datetime "published_at"
    t.string   "sku"
    t.decimal  "weight",           precision: 8, scale: 3, default: 0.0
    t.decimal  "price",            precision: 8, scale: 2, default: 0.0
    t.decimal  "cost_price",       precision: 8, scale: 2, default: 0.0
    t.boolean  "featured",                                 default: false
    t.integer  "stock",                                    default: 0
    t.integer  "likes_count",                              default: 0
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "ancho"
    t.integer  "perfil"
    t.integer  "rin"
    t.string   "tire_model_brand"
    t.integer  "performance_km"
    t.integer  "performance_cold",                         default: 5
    t.integer  "performance_dry",                          default: 5
    t.integer  "performance_wet",                          default: 5
    t.integer  "category_id"
    t.integer  "brand_id"
  end

  add_index "products", ["brand_id"], name: "index_products_on_brand_id"
  add_index "products", ["category_id"], name: "index_products_on_category_id"
  add_index "products", ["user_id"], name: "index_products_on_user_id"

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "taggings", ["product_id"], name: "index_taggings_on_product_id"
  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id"

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",        null: false
    t.string   "encrypted_password",     default: "",        null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,         null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "role",                   default: "default"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
