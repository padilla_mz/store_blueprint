class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.references :cart, index: true, foreign_key: true
      t.integer :quantity
      t.decimal :price
      t.references :product_id, index: true, foreign_key: true
      t.references :charge, index: true, foreign_key: true
      t.string :property

      t.timestamps null: false
    end
  end
end
